axios.defaults.baseURL = 'http://localhost:3000/api';
axios.defaults.headers.common['Authorization'] = '';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
const request = (url,option) => {
    return new Promise((resolve, reject) => {
        axios({
            method:option.method,
            url:url,
            data:option.data ? option.data :{},
        }).then(res =>{
            if(res.data.code === '1'){
                SuccessTip(res.data.msg)
                resolve(res)
            }else if(res.data.code === '0'){
                WarnIngTips(res.data.msg)
                return false
            }
        }).catch(err => {
            console.log(err);
            FailedTip(err.message)
            reject(err)
        });
    })
}