const http = {
    get(url,data){
        let config = {
            method:'get',
            data:data ? data : {}
        }
        return request(url,config)
    },
    post(url,data){
        let config = {
            method:'post',
            data:data ? data : {}
        }
        return request(url,config)
    }
}