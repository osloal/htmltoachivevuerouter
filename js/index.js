const Login = { 
    name:'Login',
    components:{
        
    },
    data(){
        return{
            index:"3214234235235345",
            form:{
                username:'',
                password:''
            },
            isRegister:false
        }
    },
    created(){

    },
    methods:{
        toLogin(){
            this.isRegister =!this.isRegister
        },
        async Login(){
            let res = await userLogin('/login',this.form)
            router.push({
                path:'/main',
                replace:true
            })
            window.localStorage.setItem('token',true)
            window.localStorage.setItem('UserID',res.data.data[0].uuid)
        },
        toRegister(){
            this.isRegister =!this.isRegister
        },
        async Register(){
            await userRegister('/register',this.form)
        }
    },
    template: `
    <div class="Login">
        <div class="LoginTitle">欢迎登录</div>
        <div class="LoginBox">
            <div class="UserName">
                <el-input v-model="form.username" placeholder="输入用户名" />
            </div>
            <div class="PassWord">
                <el-input v-model="form.password" type="password" placeholder="输入密码" />
            </div>
        </div>
        <div class="noAccount" v-if="!isRegister">还没账号？<span @click="toRegister">去注册</span></div>
        <div class="noAccount" v-else="isRegister">已有账号？<span @click="toLogin">去登录</span></div>
        <div class="LoginAndRegisterBtn" @click="Login" v-if="!isRegister">Login</div>
        <div class="LoginAndRegisterBtn" @click="Register" v-else="isRegister">Register</div>
    </div>` 
}
const About = { 
    data(){
        return{
            index:123,
            password:''
        }
    },
    
    methods:{
        toHome(){
            router.push({
                path:'/'
            })
        }
    },
    beforeRouteLeave(to,from,next){
        to.meta.keepAlive = true
        next(0)
    },
    template: `
    <div class="About">
        <el-input v-model="password" placeholder="输入密码" />
    </div>`
}
const DataInfo = {
    name:'datainfo',
    data(){
        return{
            ListData:[
                {
                    ExtractDate:'2023-04-13 11:01:53',                                      //提取日期
                    CompanyName:'瑞昌市正心斋玉石店',                                        //公司地址
                    Legalpersonname:'柯长剑',                                               //法人姓名
                    TelePhone:'13479278554',                                                //电话号码
                    RegisterDate:'2022-05-06',                                              //注册日期
                    OperateAddr:'江西省九江市瑞昌市立信广场A三楼负一层23商诚',                  //经营地址
                    Remark:''                                                               //备注
                },
                {
                    ExtractDate:'2023-04-13 11:01:53',                                      //提取日期
                    CompanyName:'瑞昌市正心斋玉石店',                                        //公司地址
                    Legalpersonname:'柯长剑',                                               //法人姓名
                    TelePhone:'13479278554',                                                //电话号码
                    RegisterDate:'2022-05-06',                                              //注册日期
                    OperateAddr:'江西省九江市瑞昌市立信广场A三楼负一层23商诚',                  //经营地址
                    Remark:''                                                               //备注
                },
                {
                    ExtractDate:'2023-04-13 11:01:53',                                      //提取日期
                    CompanyName:'瑞昌市正心斋玉石店',                                        //公司地址
                    Legalpersonname:'柯长剑',                                               //法人姓名
                    TelePhone:'13479278554',                                                //电话号码
                    RegisterDate:'2022-05-06',                                              //注册日期
                    OperateAddr:'江西省九江市瑞昌市立信广场A三楼负一层23商诚',                  //经营地址
                    Remark:''                                                               //备注
                },
                {
                    ExtractDate:'2023-04-13 11:01:53',                                      //提取日期
                    CompanyName:'瑞昌市正心斋玉石店',                                        //公司地址
                    Legalpersonname:'柯长剑',                                               //法人姓名
                    TelePhone:'13479278554',                                                //电话号码
                    RegisterDate:'2022-05-06',                                              //注册日期
                    OperateAddr:'江西省九江市瑞昌市立信广场A三楼负一层23商诚',                  //经营地址
                    Remark:''                                                               //备注
                },
                {
                    ExtractDate:'2023-04-13 11:01:53',                                      //提取日期
                    CompanyName:'瑞昌市正心斋玉石店',                                        //公司地址
                    Legalpersonname:'柯长剑',                                               //法人姓名
                    TelePhone:'13479278554',                                                //电话号码
                    RegisterDate:'2022-05-06',                                              //注册日期
                    OperateAddr:'江西省九江市瑞昌市立信广场A三楼负一层23商诚',                  //经营地址
                    Remark:''                                                               //备注
                },
                {
                    ExtractDate:'2023-04-13 11:01:53',                                      //提取日期
                    CompanyName:'瑞昌市正心斋玉石店',                                        //公司地址
                    Legalpersonname:'柯长剑',                                               //法人姓名
                    TelePhone:'13479278554',                                                //电话号码
                    RegisterDate:'2022-05-06',                                              //注册日期
                    OperateAddr:'江西省九江市瑞昌市立信广场A三楼负一层23商诚',                  //经营地址
                    Remark:''                                                               //备注
                },
            ],
            preTel:'tel:'
        }
    },
    template:`
        <div class="detail-top"></div>
        <div class="Logo">
            <img src="http://123.60.153.84/upload/default/20221212/f99814026fe9ddaffa29baea33ed54eb.jpg">
        </div>
        
        <div class="datainfo-item" v-for="(item ,index) in ListData" :key="item">
            <div class="datainfo-item-title">
                <div class="ExtractDate">提取日期：{{item.ExtractDate}}</div>
            </div>
            <div class="datainfo-item-body">
                <div class="CompanyName">企业名称：{{item.CompanyName}}</div>
                <div class="Legalpersonname">法人姓名：{{item.Legalpersonname}}</div>
                <div class="TelePhone">联系电话：<a :href="preTel+item.TelePhone" style="color:blue;">{{item.TelePhone}}</a></div>
                <div class="RegisterDate">注册日期：{{item.RegisterDate}}</div>
                <div class="OperateAddr">经营地址：{{item.OperateAddr}}</div>
            </div>
            <div class="datainfo-item-footer">
                <div class="Mark-item-info">备注</div>
            </div>
        </div>
    `
} 

const Setting = {
    name:'setting',
    data(){
        return{
            
        }
    },
    methods:{
        Logout(){
            window.localStorage.clear()
            router.push({
                path:'/'
            })
        }
    },
    template:`
        <div class="settingBox">
           <el-button type="primary" @click="Logout">退出登录？</el-button>
        </div>
    `
}

const Main = {
    name:'main',
    components:{
        DataInfo
    },
    data(){
        return{
            activeTab:0
        }
    },
    watch: {
        '$route': {
            handler(to, from){
                switch(to.path){
                    case '/main/datainfo':
                        this.activeTab = 0
                        break;
                    case '/main/setting':  
                        this.activeTab = 1
                        break;
                    default:
                        break;
                }
                let toIndex = to.meta.index //3
                let fromIndex = (from == undefined ? 4 : from.meta.index)
                if (toIndex > fromIndex){
                    // 设置动画名称
                    this.transitionName = 'slide-left';
                } else{
                    this.transitionName = 'slide-right';
                }
            },
            deep: true,
            immediate: true,
        }
    },
    created(){
    },
    methods:{
        toMessage(){
            router.replace('/main/datainfo')
            this.activeTab = 0
        },
        toSetting(){
            router.replace('/main/setting')
            this.activeTab = 1
        },
    },
    template:`
    <div class="Main">
        <!-- <div class="Main-top"></div> -->
        <div class="Main-middle">
            <router-view v-slot="{Component}">
                <transition :name="transitionName" appear>
                    <keep-alive v-if="$route.meta.keepAlive">
                        <component :is="Component" />
                    </keep-alive>
                </transition>  
                <transition :name="transitionName" appear>
                    <keep-alive v-if="!$route.meta.keepAlive">
                        <component :is="Component" :key="Component" />
                    </keep-alive>
                </transition> 
            </router-view>
        </div>
        <div class="Main-bottom">
            <div class="message" @click="toMessage">
                <el-icon :size="20" :class="activeTab == 0 ? 'm-iconColor active':'m-iconColor'"><Message /></el-icon>
                <span :class="activeTab == 0 ? 'm-iconNameColor active':'m-iconNameColor'">任务中心</span>
            </div>
            <div class="setting" @click="toSetting">
                <el-icon :size="20" :class="activeTab == 1 ? 's-iconColor active':'s-iconColor'"><Setting /></el-icon>
                <span :class="activeTab == 1 ? 's-iconNameColor active':'s-iconNameColor'">设置</span>
            </div>
        </div>
    </div>`,
    
}
const routes = [
    { 
        path: '/', 
        name:"login",
        component: Login,
        props:true,
        meta:{
            index:0,
            keepAlive:true
        }
    },
    { 
        path: '/about', 
        name:"about", 
        component: About,
        props:true,
        meta:{
            index:1,
            keepAlive:true
        }
    },
    { 
        path: '/main', 
        name:"main", 
        component: Main,
        props:true,
        meta:{
            index:2,
            keepAlive:true
        },
        redirect:'/main/datainfo',
        children:[
            {
                path:'/main/datainfo',
                name:'datainfo',
                component:DataInfo,
                meta:{
                    index:3,
                    keepAlive:true
                },
            },
            {
                path:'/main/setting',
                name:'setting',
                component:Setting,
                meta:{
                    index:4,
                    keepAlive:true
                },
            }
        ]
    }
]

const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes
}) 
router.beforeEach((to,from,next)=>{
    var isLogin = localStorage.getItem('token')
    if(to.path == '/'){
        if(isLogin){
            next('/main')
        }
    }else{
        if(isLogin){
            next()
        }else{
            next('/')
        }
    }
    next()
})
let App = {
    data(){
        return{
            includeList:['SubscribeconFerence', 'selectTheOwner'] ,//需要被缓存的组件
            transitionName:''
        }
    },
    watch: {
            // 使用watch 监听$router的变化
        $route (to, from) {
            // 如果to索引大于from索引,判断为前进状态,反之则为后退状态
            if (to.meta.index > from.meta.index){
                // 设置动画名称
                this.transitionName = 'slide_left';
            } else{
                this.transitionName = 'slide_right';
            }
        },
    },
    mounted() {
        window.SuccessTip = this.SuccessTip
        window.FailedTip = this.FailedTip
        window.WarnIngTips = this.WarnIngTips
    },
    methods:{
        SuccessTip(msg){
            this.$message({
                type:'success',
                message: msg,
                showClose: true,
                duration:3000
            })
        },
        WarnIngTips(msg){
            this.$message({
                type:'warning',
                message: msg,
                showClose: true,
                duration:3000
            })
        },
        FailedTip(msg){
            console.log("msg--->",msg)
            this.$message({
                type: 'error',
                message: msg,
                showClose: true,
                duration:3000
            })
        },
    }  
}
const app = Vue.createApp(App)
for ([name, comp] of Object.entries(ElementPlusIconsVue)) {
app.component(name, comp);
}
app.use(router)
app.use(ElementPlus);
app.mount('#app')