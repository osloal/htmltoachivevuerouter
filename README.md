###添加一个类似于ELMEssage的提示插件
方便封装----------->完成
<script>
    const Hello = Vue.createApp({
        template:`<Hello></Hello>`
    })
    Hello.component("Hello",{
        template:`<div class="hello">
            这里是我添加的模板内容
        </div>`,
        data(){
            return{
                msg:'这是我的信息'
            }
        }
    })
    Hello.mount("Hello")
</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <Hello></Hello>
    </div>
    <script src="https://unpkg.com/vue@next"></script>
    <script src="index.js"></script>
</body>
</html>